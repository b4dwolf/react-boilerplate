const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const extractSCSS = new ExtractTextPlugin('styles.css');
const DEVELOPMENT = process.env.NODE_ENV === 'development'
const PRODUCTION = process.env.NODE_ENV === 'production'

module.exports = {
    entry: [
        path.resolve('src', 'index.tsx')
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve('dist')
    },

    devtool: 'source-map',

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json', '.scss']
    },

    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.(ts,tsx)$/,
                loader: 'tslint-loader'
            },
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader'
            },
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader'
            },
            {
                test: /\.scss$/,
                use: DEVELOPMENT ?
                    ["style-loader", "css-loader", "sass-loader"] :
                    extractSCSS.extract(['css-loader', 'postcss-loader', 'sass-loader'])
            }
        ]
    },

    plugins: [
        extractSCSS,
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
        })
    ],

    devServer: {
        contentBase: path.resolve('dist'),
        hot: true
    }
}

if (PRODUCTION) {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true
        })
    )
}
import * as React from 'react'
import { connect, Dispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { RouterState, routerActions, RouterAction } from 'react-router-redux'

import { StoreState } from '../reducers'
import { ApplicationState } from '../reducers/application'
import { Hello } from '../components/Hello'
import { setLoading, SetLoadingAction, ApplicationActions } from '../actions/application'
import '../styles/App.scss'

interface AppProps {
    application: ApplicationState
    routing: RouterState
    setLoading: (isLoading: boolean) => SetLoadingAction
    push: (path: string) => RouterAction
}

export class App extends React.Component<AppProps> {
    setLoading = (e: React.MouseEvent<HTMLButtonElement>) => {
        console.log(e.target.dataset.path)
    }

    render() {
        const goToSecond = () => {
            this.props.push('/second')
        }
        console.log('azaza')

        return (
            <div>
                <button onClick={goToSecond}>Go to second</button>
                <Hello
                    compiler="TypeScript"
                    framework="React"
                />
                <button data-path="/second" onClick={this.setLoading}>isLoading: {this.props.application.isLoading.toString()}</button>
            </div>
        )
    }
}

function mapStateToProps(state: StoreState) {
    return {
        application: state.application,
        routing: state.routing
    }
}

function mapDispatchToProps(dispatch: Dispatch<RouterAction>) {
    return bindActionCreators(
        {
            push: routerActions.push
        },
        dispatch
    )
}

export const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(App)

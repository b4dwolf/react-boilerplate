import * as React from 'react'
import { Route } from 'react-router'
import { Switch } from 'react-router-dom'

import { AppContainer } from './App'
import { SecondContainer } from './Second'

export class Routes extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Switch>
                    <Route
                        exact={true}
                        path="/"
                        component={AppContainer}
                    />
                    <Route
                        exact={true}
                        path="/second"
                        component={SecondContainer}
                    />
                </Switch>
            </React.Fragment>
        )
    }
}

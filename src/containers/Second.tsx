import * as React from 'react'
import { connect, Dispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { RouterState, routerActions, RouterAction } from 'react-router-redux'

import { StoreState } from '../reducers'
import { ApplicationState } from '../reducers/application'

export interface SecondProps {
    routing: RouterState
    push: (path: string) => RouterAction
}

export class Second extends React.Component<SecondProps> {
    render() {
        const goBack = () => this.props.push('/')
        return (
            <div>
                <button onClick={goBack}>Go path</button>
            </div>
        )
    }
}

function mapStateToProps(state: StoreState) {
    return {
        routing: state.routing
    }
}

function mapDispatchToProps(dispatch: Dispatch<RouterAction>) {
    return bindActionCreators(
        {
            push: routerActions.push
        },
        dispatch
    )
}

export const SecondContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Second)

import { createStore, applyMiddleware, compose, Store } from 'redux'
import { default as createSagaMiddleware } from 'redux-saga'
import { routerMiddleware } from 'react-router-redux'
import { History } from 'history'
import { reducers, StoreState } from '../reducers'
import { helloSaga } from '../sagas'

export function configureStore(history: History): Store<StoreState> {
    const { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: devToolsCompose } = window
    const composeEnhancers = devToolsCompose || compose
    const sagaMiddleware = createSagaMiddleware()
    const store = createStore<StoreState>(
        reducers,
        composeEnhancers(
            applyMiddleware(
                sagaMiddleware,
                routerMiddleware(history)
            )
        )
    )
    sagaMiddleware.run(helloSaga)

    if (module.hot) {
        module.hot.accept(() => {
            store.replaceReducer(require('../reducers').reducers)
        })
    }

    return store
}
